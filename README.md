# NAME

CXC::Astro::Regions - Astronomical region support

# VERSION

version 0.04

# DESCRIPTION

**CXC::Astro::Regions** provides support for specifying and
manipulating descriptions of regions used in astronomical software.

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-cxc-astro-regions@rt.cpan.org  or through the web interface at: [https://rt.cpan.org/Public/Dist/Display.html?Name=CXC-Astro-Regions](https://rt.cpan.org/Public/Dist/Display.html?Name=CXC-Astro-Regions)

## Source

Source is available at

    https://gitlab.com/djerius/cxc-astro-regions

and may be cloned from

    https://gitlab.com/djerius/cxc-astro-regions.git

# SEE ALSO

Please see those modules/websites for more information related to this module.

- [CXC::Astro::Regions::DS9](https://metacpan.org/pod/CXC%3A%3AAstro%3A%3ARegions%3A%3ADS9)
- [CXC::Astro::Regions::CIAO](https://metacpan.org/pod/CXC%3A%3AAstro%3A%3ARegions%3A%3ACIAO)
- [CXC::Astro::Regions::CFITSIO](https://metacpan.org/pod/CXC%3A%3AAstro%3A%3ARegions%3A%3ACFITSIO)

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2023 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007

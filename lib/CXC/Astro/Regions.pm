package CXC::Astro::Regions;

# ABSTRACT: Astronomical region support

use v5.20;
use strict;
use warnings;

our $VERSION = '0.04';

1;

# COPYRIGHT

__END__

=pod

=head1 DESCRIPTION

B<CXC::Astro::Regions> provides support for specifying and
manipulating descriptions of regions used in astronomical software.

=head1 SEE ALSO

CXC::Astro::Regions::DS9

CXC::Astro::Regions::CIAO

CXC::Astro::Regions::CFITSIO

=cut

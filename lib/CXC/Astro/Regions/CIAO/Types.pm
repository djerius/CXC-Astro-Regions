package CXC::Astro::Regions::CIAO::Types;

# ABSTRACT: Types for CIAO Regions

use v5.20;
use warnings;

our $VERSION = '0.04';

use Types::Standard        qw( Enum Bool Num Str StrMatch );
use Types::Common::Numeric qw( PositiveNum );

use Type::Utils -all;
use Regexp::Common qw( number );
use Type::Library
  -base,
  -extends => [ 'Types::Common::Numeric', 'Types::Common::String', 'Types::Standard' ],
  -declare => qw(
  Angle
  Length
  PositionAsNum
  Vertex
  XPosition
  YPosition
  );

use CXC::Types::Astro::Coords 'Sexagesimal';

declare PositionAsNum,
  as StrMatch [qr{\A (?: $RE{num}{real} [d]?) \z}x];


declare XPosition, as PositionAsNum | Sexagesimal [ -ra,  -sep ];
declare YPosition, as PositionAsNum | Sexagesimal [ -dec, -sep ];
declare Vertex,    as Tuple [ XPosition, YPosition ];

declare Length, as StrMatch [qr/\A$RE{num}{real} (["d]|[']{1,2})?\z/x];

declare Angle, as Num;

1;

# COPYRIGHT

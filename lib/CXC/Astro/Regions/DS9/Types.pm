package CXC::Astro::Regions::DS9::Types;

# ABSTRACT: Types for DS9 Regions

use v5.20;
use warnings;

our $VERSION = '0.04';

use CXC::Types::Astro::Coords 'Sexagesimal';

use Types::Standard        qw( Enum Bool Num StrMatch  );
use Types::Common::Numeric qw( PositiveNum );
use Type::Utils -all;
use Regexp::Common qw( number );
use Type::Library
  -base,
  -extends => [ 'Types::Common::Numeric', 'Types::Common::String', 'Types::Standard' ],
  -declare => qw(
  Angle
  CoordSys
  Length
  LengthPair
  OneZero
  PointType
  PositionAsNum
  RulerCoords
  Vertex
  XPosition
  YPosition
  );

declare OneZero, as Enum [ 1, 0 ];
coerce OneZero, from Bool->coercibles, via { to_Bool( $_ ) ? q{1} : q{0} };

declare PositionAsNum,
  as StrMatch [qr{\A (?: $RE{num}{real} [drpi]?) \z}x];

declare XPosition,
  as PositionAsNum | Sexagesimal( [ '-ra', '-units' ] ) | Sexagesimal( [ '-ra', '-sep' ] );
declare YPosition,
  as PositionAsNum | Sexagesimal( [ '-dec', '-units' ] ) | Sexagesimal( [ '-dec', '-sep' ] );
declare Vertex, as Tuple [ XPosition, YPosition ];

declare Length,     as StrMatch [qr/$RE{num}{real} ["'drpi]?/x];
declare LengthPair, as Tuple [ Length, Length ];

declare Angle, as Num;

## no critic (RegularExpressions::ProhibitEnumeratedClasses)
declare CoordSys, as StrMatch->of( qr/wcs[a-z]/i ) | Enum->of(
    \1,    # use Type::Tiny::Enum::closest_match
    'amplifier',
    'detector',
    'ecliptic',
    'fk4',
    'fk5',
    'galactic',
    'icrs',
    'image',
    'linear',
    'physical',
  ),
  coercion => 1;

declare PointType, as Enum [
    \1,    # use Type::Tiny::Enum::closest_match
    qw( circle box diamond cross x arrow boxcircle ),
  ],
  coercion => 1;

declare RulerCoords, as Enum [
    \1,    # use Type::Tiny::Enum::closest_match
    qw( pixels degrees arcmin arcsec ),
  ],
  coercion => 1;

1;

# COPYRIGHT
